<?php

/**
 * Created by PhpStorm.
 * User: Abd Shammout
 * Date: 20/6/2022
 * Time: 12:35 AM
 */

namespace App\Traits;


trait helperTrait
{


    public function countNumberOfBeforeSelectedItems($currentRank, $per_page, $usersCount){
        if ($per_page > $usersCount)
            $per_page = $usersCount;


        if ($currentRank - intval($per_page) <= 0){
            return $numberOfElementsBeforeSelected = $currentRank-1;
        }else{
            if ($currentRank + intval($per_page/2) > $usersCount)
                return $numberOfElementsBeforeSelected = $per_page - ($usersCount - $currentRank) - 1;
            else
                return $numberOfElementsBeforeSelected = intval($per_page/2);
        }

    }
}