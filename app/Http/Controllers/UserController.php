<?php

namespace App\Http\Controllers;

use App\Http\Requests\FindRankUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\helperTrait;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    use helperTrait;

    public function findKarmaPosition(FindRankUserRequest $request){

        $userId = $request->id;
        $perPage = $request->input('per_page', Config::get('constants.api_config.per_page'));
        $returnJson = $request->input('return_Json', false);

        config()->set('database.connections.mysql.strict', false);

        $currentUser = DB::table(
            DB::table('users')
                ->select(['id'])
                ->selectRaw('RANK() OVER(ORDER BY karma_score desc) as rank_')
        )->find($userId);

        $usersCount = User::query()->count();
        $numberOfElementsBeforeSelected =
            $this->countNumberOfBeforeSelectedItems($currentUser->rank_, $perPage, $usersCount);
        $firstRankView_ = $currentUser->rank_ - $numberOfElementsBeforeSelected;

        $users = User::query()
            ->orderBy('karma_score', 'desc')
            ->offset($firstRankView_)
            ->limit($perPage)
            ->get();

        $rank_ = $firstRankView_;
        $lastKarmaScore = 0;
        foreach ($users as $user){
            $user['rank_'] = $rank_;
            if ($lastKarmaScore != $user->karma_score)
                $rank_++;
            $lastKarmaScore = $user->karma_score;
        }

        return $returnJson
            ? $this->sendResponseResource(UserResource::collection($users))
            : view('users', ["users" => $users, "currentId" => $userId]);
    }

}
