<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;



Route::prefix('users')->group(function (){
    Route::get('/{id}/karma-position', [UserController::class, 'findKarmaPosition'])
        ->name('karmaPosition');
});